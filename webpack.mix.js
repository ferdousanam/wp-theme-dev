let mix = require('laravel-mix');

mix.options({
  processCssUrls: false
}).sourceMaps();

mix.js('assets/src/js/app.js', 'assets/js/')
    .sass('assets/src/scss/custom.scss', 'assets/css/');