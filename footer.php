<footer>
    <div class="container">
        <div class="row">
            <div class="span4">
                <?php if (is_active_sidebar('eterna-footer-1')): ?>
                    <?php dynamic_sidebar( 'eterna-footer-1' ); ?>
                <?php endif; ?>
            </div>
            <div class="span4">
                <?php if (is_active_sidebar('eterna-footer-2')): ?>
                    <?php dynamic_sidebar( 'eterna-footer-2' ); ?>
                <?php endif; ?>
            </div>
            <div class="span4">
                <?php if (is_active_sidebar('eterna-footer-3')): ?>
                    <?php dynamic_sidebar( 'eterna-footer-3' ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="span6">
                    <div class="copyright">
                        <p><span>&copy; <?=get_bloginfo('name');?>. All right reserved</span></p>
                    </div>

                </div>

                <div class="span6">
                    <div class="credits">
                        Developed by <a href="https://ferdousanam.com/">Ferdous Anam</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

<!-- javascript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<?php wp_footer(); ?>

</body>

</html>