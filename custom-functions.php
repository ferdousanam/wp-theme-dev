<?php

function current_paged($var = '') {
    if (empty($var)) {
        global $wp_query;
        if (!isset($wp_query->max_num_pages))
            return;
        $pages = $wp_query->max_num_pages;
    } else {
        global $$var;
        if (!is_a($$var, 'WP_Query'))
            return;
        if (!isset($$var->max_num_pages) || !isset($$var))
            return;
        $pages = absint($$var->max_num_pages);
    }
    if ($pages < 1)
        return;
    $page = get_query_var('paged') ? get_query_var('paged') : 1;
    echo 'Page ' . $page . ' of ' . $pages;
}

function custom_pagination($pages = '', $range = 2, $prev_next_link = false, $prev_label = null, $next_label = null) {
    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged)) $paged = 1;

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        if ($prev_next_link) echo get_previous_posts_link($prev_label);
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link(1) . "'>&laquo;</a>";
        if ($paged > 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<span class='current'>" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged + 1) . "'>&rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($pages) . "'>&raquo;</a>";
        if ($prev_next_link) echo get_next_posts_link($next_label);
        echo "\n";
    }
}

function get_first_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img .+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches[1][0];
    if (empty($first_img)) {
        // defines a fallback image
        $first_img = get_template_directory_uri() . "/assets/img/dummies/blog/img1.jpg";
    }
    $first_img = '<img src="' . $first_img . '" alt="' . $post->post_title . '" />';
    return $first_img;
}