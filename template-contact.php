<?php
/**
 * Template Name: Contact Us
 */

get_header(); ?>

<?php
if (have_posts()) { // check page contains any post data or not

    while (have_posts()) { // repeat over each post of the page
        ?>
        <?php the_post(); ?>
        <section id="inner-headline">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="inner-heading">
                            <ul class="breadcrumb">
                                <li><a href="<?php echo get_site_url(); ?>">Home</a> <i class="icon-angle-right"></i></li>
                                <li class="active"><?php echo get_the_title(); ?></li>
                            </ul>
                            <h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="content">
            <div class="container">
                <div class="row">

                    <div class="span8">

                        <?php the_content(); ?>

                    </div>

                    <div class="span4">

                        <aside class="right-sidebar">

                            <?php get_sidebar(); ?>

                        </aside>
                    </div>

                </div>
            </div>
        </section>
        <?php
    }
}
?>

<?php get_footer(); ?>