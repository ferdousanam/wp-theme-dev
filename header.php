<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo get_bloginfo('name'); ?> - <?php echo get_bloginfo('description'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Your page description here"/>
    <meta name="author" content=""/>

    <!-- css -->
    <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">

    <?php wp_head(); ?>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_bloginfo('template_url'); ?>/assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_bloginfo('template_url'); ?>/assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_bloginfo('template_url'); ?>/assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_bloginfo('template_url'); ?>/assets/ico/apple-touch-icon-57-precomposed.png"/>
    <link rel="shortcut icon" href="<?php echo get_bloginfo('template_url'); ?>/assets/ico/favicon.png"/>
</head>

<body>

<div id="wrapper">

    <!-- start header -->
    <header>
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="span6">

                        <?php if (is_active_sidebar('eterna-header-top-left')): ?>
                            <?php dynamic_sidebar('eterna-header-top-left'); ?>
                        <?php endif; ?>

                    </div>
                    <div class="span6">

                        <?php if (is_active_sidebar('eterna-header-top-right')): ?>
                            <?php dynamic_sidebar('eterna-header-top-right'); ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">


            <div class="row nomargin">
                <div class="span4">
                    <div class="logo">
                        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/logo.png" alt=""/></a>
                    </div>
                </div>
                <div class="span8">
                    <div class="navbar navbar-static-top">
                        <div class="navigation">
                            <nav>
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'primary-menu',
                                    'container' => '',
                                    'menu_class' => 'nav topnav',
                                    'fallback_cb' => 'default_main_menu',
                                    'walker' => new Eterna_walker(),
                                ));
                                ?>
                            </nav>
                        </div>
                        <!-- end navigation -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- end header -->