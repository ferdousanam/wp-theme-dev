<?php
/*

@package eterna

	========================
		CATEGORY WIDGET CLASS
	========================
*/

class Eterna_Category_Widget extends WP_Widget {

    //setup the widget name, description, etc...
    public function __construct() {

        $widget_ops = array(
            'classname' => 'eterna-category-widget',
            'description' => 'Eterna Category Widget',
        );
        parent::__construct('eterna_category', 'Eterna Category', $widget_ops);

    }

    //back-end display of widget
    public function form($instance) {
        $title = esc_attr($instance['title']);
        $count = esc_attr($instance['count']);
        $note = esc_attr($instance['message']);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/>
        </p>
        <p>
            <input type="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" value="yes"<?php checked('yes', $count); ?>>
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Show post counts'); ?></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('note'); ?>"><?php _e('Note For the widget:'); ?></label>
            <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('note'); ?>" name="<?php echo $this->get_field_name('note'); ?>"><?php echo $note; ?></textarea>
        </p>

        <?php
    }

    public function update($new_instance, $old_instance) {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['count'] = isset($new_instance['count']) ? 'yes' : 'no';
        $instance['note'] = strip_tags($new_instance['note']);

        return $instance;

    }

    //front-end display of widget
    public function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        if ($title) {
            $title = $before_title . $title . $after_title;
        }
        echo $args['before_widget'];
        ?>
        <div class="widget">

            <h5 class="widgetheading"><?= $title ?></h5>

            <?php //print_r(get_the_category()) ?>
            <ul class="cat">
                <?php foreach ((get_categories()) as $category): ?>
                    <li><i class="icon-angle-right"></i>
                        <a href="<?php echo get_category_link($category); ?>"><?php echo $category->name ?></a>
                        <?php if ($instance['count'] == 'yes'): ?>
                            <span> (<?php echo $category->count; ?>)</span>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php
        echo $args['after_widget'];
    }

}

add_action('widgets_init', function () {
    register_widget('Eterna_Category_Widget');
});


/*
	Edit default WordPress widgets
*/

function eterna_tag_cloud_font_change($args) {
    $args['smallest'] = 8;
    $args['largest'] = 8;

    return $args;
}

add_filter('widget_tag_cloud_args', 'eterna_tag_cloud_font_change');