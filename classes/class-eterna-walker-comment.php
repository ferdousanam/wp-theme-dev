<?php
/**
 * Custom comment walker for this theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

if ( ! class_exists( 'Eterna_Walker_Comment' ) ) {
    /**
     * CUSTOM COMMENT WALKER
     * A custom walker for comments, based on the walker in Twenty Nineteen.
     */
    class Eterna_Walker_Comment extends Walker_Comment {

        /**
         * Outputs a comment in the HTML5 format.
         *
         * @see wp_list_comments()
         * @see https://developer.wordpress.org/reference/functions/get_comment_author_url/
         * @see https://developer.wordpress.org/reference/functions/get_comment_author/
         * @see https://developer.wordpress.org/reference/functions/get_avatar/
         * @see https://developer.wordpress.org/reference/functions/get_comment_reply_link/
         * @see https://developer.wordpress.org/reference/functions/get_edit_comment_link/
         *
         * @param WP_Comment $comment Comment to display.
         * @param int        $depth   Depth of the current comment.
         * @param array      $args    An array of arguments.
         */
        protected function html5_comment( $comment, $depth, $args ) {

            $tag = ( 'div' === $args['style'] ) ? 'div' : 'li';

            ?>
            <<?php echo $tag; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( ['media', $this->has_children ? 'parent' : ''], $comment ); ?>>
            <?php
            $comment_author_url = get_comment_author_url( $comment );
            $comment_author     = get_comment_author( $comment );
            $avatar             = get_avatar( $comment, $args['avatar_size'], '', get_comment_author(), ['class' => 'img-circle'] );
            if ( 0 !== $args['avatar_size'] ) {
                if ( empty( $comment_author_url ) ) {
                    echo '<span class="pull-left">';
                    echo wp_kses_post( $avatar );
                    echo '</span>';
                } else {
                    printf( '<a href="%s" rel="external nofollow" class="pull-left">', $comment_author_url );
                    echo wp_kses_post( $avatar );
                }
            }

            if ( ! empty( $comment_author_url ) ) {
                echo '</a>';
            }
            ?>
            <?php
            $comment_timestamp = sprintf( __( '%1$s at %2$s', 'eterna' ), get_comment_date( 'd M, Y', $comment ), get_comment_time() );
            ?>
            <div class="media-body">
                <div class="media-content">
                    <h6><span><?php echo $comment_timestamp; ?></span> <?php echo $comment_author; ?></h6>
                    <p>
                        <?php comment_text(); ?>
                    </p>
                    <?php
                    if ('0' === $comment->comment_approved) { ?>
                        <p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'twentytwenty'); ?></p>
                    <?php } ?>

                    <?php

                    $comment_reply_link = get_comment_reply_link(
                        array_merge(
                            $args,
                            array(
                                'add_below' => 'div-comment',
                                'depth'     => $depth,
                                'max_depth' => $args['max_depth'],
                                'before'    => '<div class="align-right">',
                                'after'     => '</div>',
                            )
                        )
                    );

                    if ( $comment_reply_link ) {
                        echo $comment_reply_link;
                    }
                    ?>
                </div>
                    <?php
                    if (!$this->has_children || $depth > 1) {
                        echo '</div>';
                        if (!$this->has_children && $depth > 1) {
                            echo '</div>';
                        }
                    }
        }
    }
}
