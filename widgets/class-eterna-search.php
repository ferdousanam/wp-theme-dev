<?php
/*

@package eterna

	========================
		SEARCH WIDGET CLASS
	========================
*/

class Eterna_Search_Widget extends WP_Widget {

    //setup the widget name, description, etc...
    public function __construct() {

        $widget_ops = array(
            'classname' => 'eterna-search-widget',
            'description' => 'Eterna Search Widget',
        );
        parent::__construct('eterna_search', 'Eterna Search', $widget_ops);

    }

    //back-end display of widget
    public function form($instance) {
        $title = esc_attr($instance['title']);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/>
        </p>

        <?php
    }

    public function update($new_instance, $old_instance) {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;

    }

    //front-end display of widget
    public function widget($args, $instance) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        if ($title) {
            $title = $before_title . $title . $after_title;
        }
        echo $args['before_widget'];
        ?>
        <div class="widget">
            <h5 class="widgetheading"><?= $title ?></h5>
            <form role="search" method="get" action="<?= esc_url(home_url('/')); ?>">
                <div class="input-append">
                    <input class="span2" id="appendedInputButton" type="text" placeholder="<?= esc_attr_x( 'Search &hellip;', 'placeholder', 'eterna' ); ?>" value="<?= get_search_query(); ?>" name="s">
                    <button class="btn btn-theme" type="submit"><?= _x( 'Search', 'submit button', 'eterna' ); ?></button>
                </div>
            </form>
        </div>
        <?php
        echo $args['after_widget'];
    }

}

add_action('widgets_init', function () {
    register_widget('Eterna_Search_Widget');
});