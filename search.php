<?php get_header(); ?>

    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="inner-heading">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo get_site_url(); ?>">Home</a> <i class="icon-angle-right"></i></li>
                            <li class="active"><?php echo get_search_query(); ?></li>
                        </ul>
                        <h2>Search: <?php echo get_search_query(); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="row">

                <div class="span8">
                    <?php
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post(); ?>
                            <article>
                                <div class="row">

                                    <div class="span8">
                                        <div class="post-image">
                                            <div class="post-heading">
                                                <h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                                            </div>
                                            <?php
                                            if (get_the_post_thumbnail(get_the_ID()) != '') {
                                                the_post_thumbnail();
                                            } else {
                                                echo get_first_image();
                                            }
                                            ?>
                                        </div>
                                        <div class="meta-post">
                                            <ul>
                                                <li><i class="icon-file"></i></li>
                                                <li>By <?php the_author_posts_link(); ?></li>
                                                <li>On <a href="#" class="date"><?php the_date(); ?></a></li>
                                                <li><?php the_tags(); ?></a></li>
                                            </ul>
                                        </div>
                                        <div class="post-entry">
                                            <p><?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php }
                    } else { ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php }
                    ?>

                    <div id="pagination">
                        <span class="all"><?php current_paged(); ?></span>
                        <?php //echo paginate_links(); ?>
                        <?php custom_pagination('', 3); ?>

                    </div>
                </div>

                <div class="span4">

                    <aside class="right-sidebar">

                        <?php get_sidebar(); ?>

                    </aside>
                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>