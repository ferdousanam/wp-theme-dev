<?php

require_once('eterna-nav-walker.php');
require_once('custom-functions.php');
require_once get_template_directory() . '/includes/theme-support.php'; // Different way of require file
require_once get_template_directory() . '/includes/theme-filters.php';
require_once get_template_directory() . '/includes/theme-menus.php';
require_once('classes/class-comment-walker.php');
require_once('classes/class-eterna-walker-comment.php');
require_once('classes/class-eterna-category-widgets.php');
require_once('widgets/class-eterna-search.php');

function eterna_enqueue_style() {
    wp_enqueue_style('et-theme', get_stylesheet_uri());
    wp_enqueue_style('et-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css');
    wp_enqueue_style('et-bootstrap-responsive', get_template_directory_uri() . '/assets/css/bootstrap-responsive.css');
    wp_enqueue_style('et-flexslider', get_template_directory_uri() . '/assets/css/flexslider.css');
    wp_enqueue_style('et-prettyPhoto', get_template_directory_uri() . '/assets/css/prettyPhoto.css');
    wp_enqueue_style('et-camera', get_template_directory_uri() . '/assets/css/camera.css');
    wp_enqueue_style('et-jquery-bxslider', get_template_directory_uri() . '/assets/css/jquery.bxslider.css');
    wp_enqueue_style('et-style', get_template_directory_uri() . '/assets/css/style.css');
    wp_enqueue_style('et-default', get_template_directory_uri() . '/assets/color/default.css');
    wp_enqueue_style('et-custom', get_template_directory_uri() . '/assets/css/custom.css');
}

function eterna_enqueue_script() {
    wp_enqueue_script('jquery-js', get_template_directory_uri() . '/assets/js/jquery.js', array(), '1.1', true);
    wp_enqueue_script('jquery-easing-js', get_template_directory_uri() . '/assets/js/jquery.easing.1.3.js', array(), '1.3', true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.js', array(), '1.1', true);
    wp_enqueue_script('modernizr-custom-js', get_template_directory_uri() . '/assets/js/modernizr.custom.js', array(), '1.1', true);
    wp_enqueue_script('toucheffects-js', get_template_directory_uri() . '/assets/js/toucheffects.js', array(), '1.1', true);
    wp_enqueue_script('prettify-js', get_template_directory_uri() . '/assets/js/google-code-prettify/prettify.js', array(), '1.1', true);
    wp_enqueue_script('jquery-bxslider-js', get_template_directory_uri() . '/assets/js/jquery.bxslider.min.js', array(), '1.1', true);
    wp_enqueue_script('jquery-prettyPhoto-js', get_template_directory_uri() . '/assets/js/jquery.prettyPhoto.js', array(), '1.1', true);
    wp_enqueue_script('jquery-quicksand-js', get_template_directory_uri() . '/assets/js/portfolio/jquery.quicksand.js', array(), '1.1', true);
    wp_enqueue_script('setting-js', get_template_directory_uri() . '/assets/js/portfolio/setting.js', array(), '1.1', true);
    wp_enqueue_script('jquery-flexslider-js', get_template_directory_uri() . '/assets/js/jquery.flexslider.js', array(), '1.1', true);
    wp_enqueue_script('animate-js', get_template_directory_uri() . '/assets/js/animate.js', array(), '1.1', true);
    wp_enqueue_script('inview-js', get_template_directory_uri() . '/assets/js/inview.js', array(), '1.1', true);
    wp_enqueue_script('custom-js', get_template_directory_uri() . '/assets/js/custom.js', array(), '1.1', true);
}

add_action('wp_enqueue_scripts', 'eterna_enqueue_style');
add_action('wp_enqueue_scripts', 'eterna_enqueue_script');