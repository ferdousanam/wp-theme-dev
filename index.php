<?php get_header(); ?>

    <section id="content">
        <div class="container">
            <div class="row">

                <div class="span8">
                    <?php
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post(); ?>
                            <article>
                                <div class="row">

                                    <div class="span8">
                                        <div class="post-image">
                                            <div class="post-heading">
                                                <h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                                            </div>
                                            <?php
                                            if (get_the_post_thumbnail(get_the_ID()) != '') {
                                                the_post_thumbnail();
                                            } else {
                                                echo get_first_image();
                                            }
                                            ?>
                                        </div>
                                        <div class="meta-post">
                                            <ul>
                                                <li><i class="icon-file"></i></li>
                                                <li>By <?php the_author_posts_link(); ?></li>
                                                <li>On <a href="#" class="date"><?php the_date(); ?></a></li>
                                                <li><?php the_tags(); ?></a></li>
                                            </ul>
                                        </div>
                                        <div class="post-entry">
                                            <p><?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php }
                    } else { ?>
                        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php }
                    ?>

                    <div id="pagination">
                        <span class="all"><?php current_paged(); ?></span>
                        <?php //echo paginate_links(); ?>
                        <?php custom_pagination('', 3); ?>

                    </div>
                </div>

                <div class="span4">

                    <aside class="right-sidebar">

                        <?php get_sidebar(); ?>

                        <div class="widget">
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#one" data-toggle="tab"><i class="icon-star"></i> Popular</a></li>
                                    <li><a href="#two" data-toggle="tab">Recent</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="one">
                                        <ul class="popular">
                                            <li>
                                                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dummies/blog/small/1.jpg" alt="" class="thumbnail pull-left" />
                                                <p><a href="#">Dorlorem ipsum et mea dolor sit amet</a></p>
                                                <span>20 June, 2013</span>
                                            </li>
                                            <li>
                                                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dummies/blog/small/2.jpg" alt="" class="thumbnail pull-left" />
                                                <p><a href="#">Fierent adipisci iracundia est ei, usu timeam persius ea</a></p>
                                                <span>20 June, 2013</span>
                                            </li>
                                            <li>
                                                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dummies/blog/small/3.jpg" alt="" class="thumbnail pull-left" />
                                                <p><a href="#">Usu ea justo malis, pri quando everti electram ei</a></p>
                                                <span>20 June, 2013</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" id="two">
                                        <ul class="recent">
                                            <li>
                                                <p><a href="#">Dorlorem ipsum et mea dolor sit amet</a></p>
                                            </li>
                                            <li>
                                                <p><a href="#">Fierent adipisci iracundia est ei, usu timeam persius ea</a></p>
                                            </li>
                                            <li>
                                                <p><a href="#">Usu ea justo malis, pri quando everti electram ei</a></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="widget">

                            <h5 class="widgetheading">Video widget</h5>
                            <div class="video-container">
                                <iframe src="http://player.vimeo.com/video/30585464?title=0&amp;byline=0">			</iframe>
                            </div>
                        </div>

                        <div class="widget">
                            <h5 class="widgetheading">Flickr photostream</h5>
                            <div class="flickr_badge">
                                <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </aside>
                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>