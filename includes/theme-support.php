<?php
/*

@package eterna

	========================
		THEME SUPPORT OPTIONS
	========================
*/

/* Activate HTML5 features */
add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));

/*
	========================
		HEADER TOP FUNCTIONS
	========================
*/
function eterna_header_top_init() {

    register_sidebar(
        array(
            'name' => esc_html__('Eterna Header Top Left', 'eterna'),
            'id' => 'eterna-header-top-left',
            'description' => 'Dynamic Header Top Left',
            'before_widget' => '<span id="%1$s">',
            'after_widget' => '</span>',
            'before_title' => '',
            'after_title' => ''
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Eterna Header Top Right', 'eterna'),
            'id' => 'eterna-header-top-right',
            'description' => 'Dynamic Header Top Right',
            'before_widget' => '<span id="%1$s">',
            'after_widget' => '</span>',
            'before_title' => '',
            'after_title' => ''
        )
    );

}

add_action('widgets_init', 'eterna_header_top_init');

/*
	========================
		POST THUMBNAILS FUNCTIONS
	========================
*/

add_theme_support( 'post-thumbnails' );

/*
	========================
		IMAGE SIZE FUNCTIONS
	========================
*/

add_image_size('single_post_image', 870, 350, false);

/*
	========================
		SIDEBAR FUNCTIONS
	========================
*/
function eterna_sidebar_init() {

    register_sidebar(
        array(
            'name' => esc_html__('Eterna Sidebar', 'eterna'),
            'id' => 'eterna-sidebar',
            'description' => 'Dynamic Right Sidebar',
            'before_widget' => '<div id="%1$s" class="widgets %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widgetheading">',
            'after_title' => '</h5>'
        )
    );

}

add_action('widgets_init', 'eterna_sidebar_init');

/*
	========================
		FOOTER FUNCTIONS
	========================
*/
function eterna_footer_init() {

    register_sidebar(
        array(
            'name' => esc_html__('Eterna Footer 1', 'eterna'),
            'id' => 'eterna-footer-1',
            'description' => 'Eterna Footer 1',
            'before_widget' => '<div id="%1$s" class="widgets %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widgetheading">',
            'after_title' => '</h5>'
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Eterna Footer 2', 'eterna'),
            'id' => 'eterna-footer-2',
            'description' => 'Eterna Footer 2',
            'before_widget' => '<div id="%1$s" class="widgets %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widgetheading">',
            'after_title' => '</h5>'
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Eterna Footer 3', 'eterna'),
            'id' => 'eterna-footer-3',
            'description' => 'Eterna Footer 3',
            'before_widget' => '<div id="%1$s" class="widgets %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widgetheading">',
            'after_title' => '</h5>'
        )
    );

}

add_action('widgets_init', 'eterna_footer_init');