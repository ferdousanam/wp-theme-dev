<?php

function modify_read_more_link() {
    return '<a href="' . the_permalink() . '" class="readmore">Read more <i class="icon-angle-right"></i></a>';
}
add_filter('the_content_more_link', 'modify_read_more_link');

function modify_excerpt_more($more) {
    global $post;
    return '</p><a class="readmore" href="' . get_permalink($post->ID) . '">Read more <i class="icon-angle-right"></i></a>';
}
add_filter('excerpt_more', 'modify_excerpt_more');
