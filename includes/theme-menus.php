<?php

function register_eterna_theme() {
    // menu register code
    register_nav_menus(
        array(
            'primary-menu' => __('Primary Menu'), // Id and Name
            'footer-menu' => __('Footer Menu') // Id and Name
        )
    );
}
add_action('after_setup_theme', 'register_eterna_theme'); // attach with action hook

// Default Fallback
function default_main_menu() {
    echo '<li class="dropdown">';
    echo '<a href="' . esc_url(home_url()) . '"><i class="icon-home"></i> Home</a>';
    echo '</li>';
}