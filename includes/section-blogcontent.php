<article class="single">
    <div class="row">

        <div class="span8">
            <div class="post-image">
                <div class="post-heading">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </div>
                <div class="featured-image">
                    <?php
                    if (get_the_post_thumbnail(get_the_ID()) != '') {
                        the_post_thumbnail('single_post_image');
                    } else {
                        echo get_first_image();
                    }
                    ?>
                </div>
            </div>
            <div class="meta-post">
                <ul>
                    <li><i class="icon-file"></i></li>
                    <li>By <a href="<?php the_author_meta('user_url'); ?>" class="author"><?php the_author(); ?></a></li>
                    <li>On <a href="#" class="date"><?php the_date(); ?></a></li>
                    <li>Tags: <?php the_tags(); ?></li>
                </ul>
            </div>
            <p>
                <?php the_content(); ?>
            </p>
            <?php wp_link_pages(); ?>
        </div>
    </div>
</article>