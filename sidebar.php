<?php

if (!is_active_sidebar('eterna-sidebar')) {
    return;
}

?>

<?php dynamic_sidebar('eterna-sidebar'); ?>