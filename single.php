<?php get_header(); ?>

<?php
if (have_posts()) { // check page contains any post data or not

    while (have_posts()) { // repeat over each post of the page
        ?>
        <?php the_post(); ?>
        <section id="inner-headline">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="inner-heading">
                            <ul class="breadcrumb">
                                <li><a href="<?php echo get_site_url(); ?>">Home</a> <i class="icon-angle-right"></i></li>
                                <li class="active"><?php echo get_the_title(); ?></li>
                            </ul>
                            <h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="content">
            <div class="container">
                <div class="row">

                    <div class="span8">

                        <?php get_template_part('includes/section', 'blogcontent'); ?>

                        <!-- author info -->
                        <div class="about-author">
                            <a href="#" class="thumbnail align-left"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/avatar.png" alt="" /></a>
                            <h5><strong><?php the_author_link(); ?></strong></h5>
                            <p>
                                Qui ut ceteros comprehensam. Cu eos sale sanctus eligendi, id ius elitr saperet, ocurreret pertinacia pri an. No mei nibh consectetuer, semper ad qui, est rebum nulla argumentum ei.
                            </p>
                        </div>


                        <?php
                        if (comments_open() || get_comments_number()) {
                            comments_template();
                        }
                        ?>

                    </div>

                    <div class="span4">

                        <aside class="right-sidebar">

                            <?php get_sidebar(); ?>

                            <div class="widget">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#one" data-toggle="tab"><i class="icon-star"></i> Popular</a></li>
                                        <li><a href="#two" data-toggle="tab">Recent</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="one">
                                            <ul class="popular">
                                                <li>
                                                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dummies/blog/small/1.jpg" alt="" class="thumbnail pull-left" />
                                                    <p><a href="#">Dorlorem ipsum et mea dolor sit amet</a></p>
                                                    <span>20 June, 2013</span>
                                                </li>
                                                <li>
                                                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dummies/blog/small/2.jpg" alt="" class="thumbnail pull-left" />
                                                    <p><a href="#">Fierent adipisci iracundia est ei, usu timeam persius ea</a></p>
                                                    <span>20 June, 2013</span>
                                                </li>
                                                <li>
                                                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/dummies/blog/small/3.jpg" alt="" class="thumbnail pull-left" />
                                                    <p><a href="#">Usu ea justo malis, pri quando everti electram ei</a></p>
                                                    <span>20 June, 2013</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="two">
                                            <ul class="recent">
                                                <li>
                                                    <p><a href="#">Dorlorem ipsum et mea dolor sit amet</a></p>
                                                </li>
                                                <li>
                                                    <p><a href="#">Fierent adipisci iracundia est ei, usu timeam persius ea</a></p>
                                                </li>
                                                <li>
                                                    <p><a href="#">Usu ea justo malis, pri quando everti electram ei</a></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="widget">

                                <h5 class="widgetheading">Video widget</h5>
                                <div class="video-container">
                                    <iframe src="http://player.vimeo.com/video/30585464?title=0&amp;byline=0">			</iframe>
                                </div>
                            </div>

                            <div class="widget">
                                <h5 class="widgetheading">Flickr photostream</h5>
                                <div class="flickr_badge">
                                    <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </aside>
                    </div>

                </div>
            </div>
        </section>
        <?php
    }
}
?>

<?php get_footer(); ?>